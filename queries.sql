CREATE USER 'dragonsUW' IDENTIFIED BY 'dragons';

CREATE DATABASE dragonsUW CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON dragonsUW.* TO 'dragonsUW';

USE dragonsUW;


Create table dragons (Dragon_ID int not null primary key auto_increment, dragon_name varchar(256), colour varchar(256), wingspan int);

Create table lands (Land_ID int not null primary key auto_increment, land_name varchar(256));

CREATE TABLE dragons_lands (Dragon_ID INT, Land_ID INT);

ALTER TABLE dragons_lands ADD CONSTRAINT dragons_FK FOREIGN KEY (Dragon_ID) REFERENCES dragons (Dragon_ID);

ALTER TABLE dragons_lands ADD CONSTRAINT lands_FK FOREIGN KEY (Land_ID) REFERENCES lands (Land_ID);

Create table eggs (Egg_ID int Unique not null primary key auto_increment, weight int, diameter int, Ornaments_ID int);

Create table ornaments (Ornaments_ID int not null primary key auto_increment, colour varchar (256), ornament varchar(256));

ALTER TABLE eggs ADD CONSTRAINT ornaments_FK FOREIGN KEY (Ornaments_ID) REFERENCES ornaments (Ornaments_ID);

alter table eggs add column ornament_colour varchar(256) references ornaments(colour);

alter table eggs add column ornament_type varchar(256) references ornaments(ornament);

alter table eggs add column dragons_names varchar(256) references dragons (name);

insert into dragons (dragon_name, colour, wingspan) values ('Dygir', 'green', '200'),('Bondril', 'red', '100'),('Onosse', 'black','250'),('Chiri','yellow','50'),('Lase','blue','300');

insert into ornaments (colour,ornament) values ('pink','mesh'), ('black','striped'),('yellow','dotted'),('blue','dotted'),('green','striped'),('red','mesh');

insert into eggs (weight, diameter) values ('300','20'),('340','30'),('200','10'),('230','20'),('300','25'),('200','15');

insert into lands(land_name) values ('Froze'),('Oswia'),('Oscyae'),('Oclurg');

insert into dragons_lands (Dragon_ID, Land_ID) values ((SELECT Dragon_ID FROM dragons where dragon_name = 'Dygir'), (SELECT Land_ID FROM lands where land_name = 'Froze'));

insert into dragons_lands (Dragon_ID, Land_ID) values ((SELECT Dragon_ID FROM dragons where dragon_name = 'Dygir'), (SELECT Land_ID FROM lands where land_name = 'Oswia'));

insert into dragons_lands (Dragon_ID, Land_ID) values ((SELECT Dragon_ID FROM dragons where dragon_name = 'Bondril'), (SELECT Land_ID FROM lands where land_name = 'Froze'));

insert into dragons_lands (Dragon_ID, Land_ID) values ((SELECT Dragon_ID FROM dragons where dragon_name = 'Onosse'), (SELECT Land_ID FROM lands where land_name = 'Oswia'));

insert into dragons_lands (Dragon_ID, Land_ID) values ((SELECT Dragon_ID FROM dragons where dragon_name = 'Chiri'), (SELECT Land_ID FROM lands where land_name = 'Oswia'));

update eggs set ornament_colour='pink' where diameter=20 and weight=300;
update eggs set ornament_colour='black' where diameter=30;
update eggs set ornament_type='mesh' where diameter=20 and weight=300; 
update eggs set ornament_type='striped' where diameter=30;
update eggs set ornament_colour='yellow' where diameter=10;
update eggs set ornament_type='dotted' where diameter=10;
update eggs set ornament_colour='blue' where diameter=20 and weight=230;
update eggs set ornament_type='dotted' where diameter=20 and weight=230;
update eggs set ornament_colour='green' where diameter=25;
update eggs set ornament_type='striped' where diameter=25;
update eggs set ornament_colour='red' where diameter=15;
update eggs set ornament_type='mesh' where diameter=15;
update eggs set dragons_names='Dygir' where diameter=20 and weight=300;
update eggs set dragons_names='Dygir' where diameter=30;
update eggs set dragons_names='Bondril' where diameter=10;
update eggs set dragons_names='Onosse' where diameter=20 and weight=230;
update eggs set dragons_names='Onosse' where diameter=25;
update eggs set dragons_names='Onosse' where diameter=15;

CREATE VIEW eggs_ornaments AS SELECT weight, diameter, ornament_colour, ornament_type FROM eggs;

create view dragon_land as select d.Dragon_ID, d.dragon_name, l.Land_ID, l.land_name from dragons as d inner join dragons_lands as dl on d.dragon_id=dl.dragon_id inner join lands as l on dl.land_id=l.land_id;

create view dragon_eggs AS SELECT dragons_names, weight, diameter from eggs; 

create view dragons_no_eggs as SELECT * FROM dragons LEFT JOIN eggs ON dragons_names = dragon_name WHERE Egg_ID IS NULL;

create view dragons_size as select * from dragons where wingspan BETWEEN 200 and 400;

select * from dragons;
select * from lands;
select * from dragons_lands;
select * from eggs;
select * from ornaments;
select * from dragon_eggs;
select * from eggs_ornaments;
select * from dragon_land;
select * from dragons_no_eggs;
select * from dragons_size;

SET FOREIGN_KEY_CHECKS = 0;
drop table if exists dragons;
drop table if exists lands;
drop table if exists ornaments;
drop table if exists eggs;
drop table if exists dragons_lands;
drop view if exists dragon_eggs;
drop view if exists eggs_ornaments;
drop view if exists dragon_land;
drop view if exists dragons_no_eggs;
drop view if exists dragons_size;
